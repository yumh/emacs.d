;;; init.el --- My emacs configuration
;;; Commentary:

;;; Code:

(require 'package)

(setq package-archives '(("GNU ELPA" . "https://elpa.gnu.org/packages/")
                         ("MEPA" . "https://melpa.org/packages/"))
      inhibit-startup-screen t ; hide the startup screen
      use-package-always-ensure t

      ;; indent-tabs-mode nil
      )

(package-initialize)

;; useful to escape
(defun minibuffer-keyboard-quit ()
  "Abort recursive edit.
In Delete Selection mode, if the mark is active, just deactivate it;
then it takes a second \\[keyboard-quit] to abort the minibuffer."
  (interactive)
  (if (and delete-selection-mode transient-mark-mode mark-active)
      (setq deactivate-mark  t)
    (when (get-buffer "*Completions*") (delete-windows-on "*Completions*"))
    (abort-recursive-edit)))

;; use package is required!
(require 'use-package)

(use-package projectile)

(use-package counsel-projectile
  :commands (counsel-projectile-switch-project
             counsel-projectile-find-file
             counsel-projectile-switch-to-buffer)
  :bind* (("M-o" . counsel-projectile-find-file)
          ("M-b" . counsel-projectile-switch-to-buffer)))

(use-package ivy
  :defer 1
  :config (progn (ivy-mode 1)
                 (define-key ivy-minibuffer-map [escape]'minibuffer-keyboard-quit)
                 (setq ;ivy-use-virtual-buffers t
                  ivy-height 15
                  ivy-initial-inputs-alist nil
                  ivy-re-builders-alist '((t . ivy--regex-fuzzy)))))

(use-package counsel
  :bind* (("M-x" . counsel-M-x)
          ("M-B" . ivy-switch-buffer)
          ("M-f" . counsel-find-file)
          ("M-i" . counsel-imenu)))

(use-package flx
  :after (ivy))

(use-package swiper)

(use-package evil
  :config (progn (evil-mode 1)

                 ;; escape from everything
                 (define-key evil-normal-state-map [escape] 'keyboard-quit)
                 (define-key evil-visual-state-map [escape] 'keyboard-quit)
                 (define-key minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
                 (define-key minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
                 (define-key minibuffer-local-completion-map [escape] 'minibuffer-keyboard-quit)
                 (define-key minibuffer-local-must-match-map [escape] 'minibuffer-keyboard-quit)
                 (define-key minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)

                 (setq evil-insert-state-message nil)

                 ;; some key
                 (define-key evil-normal-state-map " e" 'find-file)
                 (define-key evil-normal-state-map " o" 'counsel-projectile-find-file)
                 (define-key evil-normal-state-map " p" 'counsel-projectile-switch-project)
                 (define-key evil-normal-state-map " s" 'save-buffer)
                 (define-key evil-normal-state-map " E" 'flycheck-list-errors)

                 (define-key evil-normal-state-map " /" 'swiper)
                 (define-key evil-normal-state-map " x" 'counsel-M-x)

                 (define-key evil-normal-state-map " -" 'evil-window-split)
                 (define-key evil-normal-state-map " v" 'evil-window-vsplit)
                 (define-key evil-normal-state-map " q" 'evil-quit)

                 (define-key evil-normal-state-map " b" 'ivy-switch-buffer)

                 (define-key evil-normal-state-map " P" (lambda ()
                                                          (interactive)
                                                          (org-latex-export-to-pdf)
                                                          (shell-command "pkill -SIGHUP mupdf")))

                 (define-key evil-normal-state-map " h" 'evil-window-left)
                 (define-key evil-normal-state-map " j" 'evil-window-down)
                 (define-key evil-normal-state-map " k" 'evil-window-up)
                 (define-key evil-normal-state-map " l" 'evil-window-right)

                 (define-key evil-normal-state-map (kbd "C-u") 'evil-scroll-up)
                 (define-key evil-normal-state-map (kbd "C-d") 'evil-scroll-down)))

(use-package evil-commentary
  :defer 2
  :config (evil-commentary-mode))

(use-package evil-surround
  :defer 2
  :config (global-evil-surround-mode 1))

(use-package evil-cleverparens
  :commands evil-cleverparens-mode
  :init (progn (add-hook 'emacs-lisp-mode-hook 'evil-cleverparens-mode)
               (add-hook 'lisp-mode-hook 'evil-cleverparens-mode)

               (add-hook 'clojure-mode-hook 'evil-cleverparens-mode)
               (add-hook 'clojurec-mode-hook 'evil-cleverparens-mode)
               (add-hook 'clojurescript-mode-hook 'evil-cleverparens-mode)))

(use-package vi-tilde-fringe
  :config (global-vi-tilde-fringe-mode))

(use-package flycheck
  :diminish ""
  :init (global-flycheck-mode))

(use-package company
  :diminish ""
  :config (progn (add-hook 'after-init-hook 'global-company-mode)
                 (define-key company-active-map (kbd "TAB")       'company-complete-common-or-cycle)
                 (define-key company-active-map (kbd "<tab>")     'company-complete-common-or-cycle)
                 (define-key company-active-map (kbd "S-TAB")     'company-select-previous)
                 (define-key company-active-map (kbd "<backtab>") 'company-select-previous)
                 (define-key company-active-map (kbd "C-n")       'company-complete-common-or-cycle)
                 (define-key company-active-map (kbd "C-p")       'company-select-previous)))

(use-package irony
  :config (progn (add-hook 'c-mode-hook 'irony-mode)
		 (add-hook 'c++-mode-hook 'irony-mode)
		 (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)))

(use-package irony-eldoc
  :after (irony)
  :config (add-hook 'irony-mode-hook #'irony-eldoc))

(use-package company-irony
  :after (company)
  :config (add-to-list 'company-backends 'company-irony))

(use-package yasnippet
  :defer 1
  :config (progn (yas-global-mode 1)
                 (global-set-key (kbd "C-c y") 'company-yasnippet)
                 (yas-load-directory "~/.emacs.d/snippets/c-mode" t)
                 (yas-load-directory "~/.emacs.d/snippets/css-mode" t)
                 (yas-load-directory "~/.emacs.d/snippets/clojure-mode" t)))

(use-package paredit
  :diminish "()"
  :config (progn (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
                 (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
                 (add-hook 'ielm-mode-hook 'enable-paredit-mode)
                 (add-hook 'lisp-mode-hook 'enable-paredit-mode)
                 (add-hook 'clojure-mode-hook 'enable-paredit-mode)
                 (add-hook 'clojurescript-mode-hook 'enable-paredit-mode)))

;; companion function to change theme:

(defun load-light-theme ()
  "Load a light theme."
  (interactive)
  (load-theme 'doom-opera-light t)
  (set-face-attribute 'font-lock-comment-face nil :weight 'normal :slant 'italic :foreground "#888")
  (set-face-attribute 'font-lock-doc-face nil :weight 'normal :foreground "#5e8f86")
  (set-face-attribute 'font-lock-type-face nil :weight 'normal :slant 'italic :foreground "#a77e04")
  (set-face-attribute 'font-lock-variable-name-face nil :weight 'normal :foreground "#c6588c")

  ;; it's different from the ones below (this set the foreground to #fff)
  (set-face-attribute 'mode-line nil :background "#f0544c" :foreground "#fff")
  (doom-themes-org-config))

(defun load-dark-theme ()
  "Load the dark theme and fixes the background."
  (interactive)
  (load-theme 'doom-peacock t)
  (set-background-color "#151515")
  (set-face-attribute 'font-lock-comment-face nil :weight 'normal :slant 'italic)
  (set-face-attribute 'font-lock-type-face nil :weight 'normal :slant 'italic :foreground "#a77e04")
  (set-face-attribute 'mode-line nil :background "#f0544c" :foreground "#000")
  (doom-themes-org-config))

;; theme pack
(use-package doom-themes
  ;; fix for loading error
  :preface (defvar doom-nord-region-highlight t)
  :config (progn (setq doom-themes-enable-bold t
                       doom-themes-enable-italic t)
                 (menu-bar-mode -1)
                 (scroll-bar-mode -1)
                 (tool-bar-mode -1)
                 (add-to-list 'default-frame-alist '(alpha . (90 . 90)))
                 ;; (add-to-list 'default-frame-alist '(font . "-misc-tamsyn-medium-r-normal--14-101-100-100-c-70-iso8859-1"))
                 ;; (add-to-list 'default-frame-alist '(font . "-misc-tamsyn-medium-r-normal--15-108-100-100-c-80-iso8859-1"))
                 (add-to-list 'default-frame-alist '(font . "IBM Plex Mono Text:size=12"))
                 (load-dark-theme)))

(use-package rainbow-delimiters
  :commands (rainbow-delimiters-mode)
  :init (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package diminish
  :config (diminish 'undo-tree-mode "UT"))

(use-package web-mode
  :mode (("\\.html\\'" . web-mode)
	 ("\\.xml\\'" . web-mode)
         ("\\.php\\'" . web-mode)
         ("\\.vue\\'" . web-mode)
         ("\\.json\\'" . web-mode))
  :commands web-mode
  :config (setq web-mode-code-indent-offset 4
                web-mode-markup-indent-offset 4
		web-mode-enable-auto-closing t
		web-mode-tag-auto-close-style 2))

;; (use-package stylus-mode
;;   :ensure f
;;   :commands (stylus-mode)
;;   :mode (("\\.styl\\'" . stylus-mode)))
;; (load-file "~/.emacs.d/stylus-mode.el")

(defun load-stylus ()
  "Load stylus-mode.el and active the mode."
  (interactive)
  (load-file "~/.emacs.d/stylus-mode.el")
  (stylus-mode))

(use-package js2-mode
  :mode ("\\.js\\'" . js2-mode)
  :interpreter "node")

;; (use-package rjsx-mode
;;   :mode ("\\.jsx\\'" . rjsx-mode))

;; (use-package tern
;;   :after (js2-mode rjsx-mode))

;; (use-package nodejs-repl
;;   :commands (nodejs-repl))

(use-package markdown-mode
  :mode ("\\.md\\'" . gfm-mode))

(use-package clojure-mode
  :mode (("\\.clj\\'" . clojure-mode)
         ("\\.cljs\\'" . clojurescript-mode)
         ("\\.cljc\\'" . clojurec-mode))
  :config (define-clojure-indent
	    ;; morse
	    (command 'defun)
	    (message 'defun)
	    (inline 'defun)
	    (callback 'defun)

	    ;; others
            (s/def 'defun)
            (s/fdef 'defun)
            (defroutes 'defun)
            (GET 2)
            (POST 2)
            (PUT 2)
            (DELETE 2)
            (HEAD 2)
            (ANY 2)
            (OPTIONS 2)
            (PATCH 2)
            (rfn 2)
            (let-routes 1)
            (context 1)))

(use-package cider
  :diminish "cider"
  :after clojure-mode
  :config (setq cider-lein-parameters "repl :headless :host localhost"))

(use-package racket-mode)

;;(use-package haskell-mode
;;  :mode "\\.hs\\'"
;;  :interpreter "haskell")

;; (use-package go-mode
;;   :mode "\\.go\\'")

;; (use-package dumb-jump
;;   :bind (("M-g o" . dumb-jump-go-other-window)
;;          ("M-g j" . dumb-jump-go)
;;          ("M-g i" . dumb-jump-go-prompt)
;;          ("M-g x" . dumb-jump-go-prefer-external)
;;          ("M-g z" . dumb-jump-go-prefer-external-other-window))
;;   :init (setq dumb-jump-selector 'ivy))

(use-package restclient
  :commands restclient-mode
  :magic ("request" . restclient-mode))

;; (use-package gnuplot-mode
;;   :mode (("\\.\\(gp\\|gnuplot\\)$" . gnuplot-mode)))

(use-package magit
  :bind ("C-x g" . magit-status))

(use-package editorconfig
  :diminish
  :defer 1
  :config (editorconfig-mode 1))

;; (use-package writegood-mode
;;   :bind (("C-c g" . 'writegood-mode)
;;          ("C-c C-g g" . 'writegood-grade-level)
;;          ("C-c C-g G" . 'writegood-rading-ease)))

;;; misc config

(add-hook 'minibuffer-setup-hook ; C-w in minibuffer
          (lambda ()
            (local-set-key (kbd "C-w") 'backward-kill-word)))

;; gpg
(setenv "GPG_AGENT_INFO" nil)
(setq epa-pinentry-mode 'loopback
      epg-gpg-program "/usr/local/bin/gpg2")
;(pinentry-start)

;; org
(add-hook 'org-mode-hook 'auto-fill-mode)
(add-hook 'org-mode-hook 'flyspell-mode)
(add-hook 'org-mode-hook (lambda () (ispell-change-dictionary "italiano")))

;; highlight code in exported pdf (require py-pygments)
(require 'ox-latex)
(add-to-list 'org-latex-packages-alist '("" "minted"))
(setq org-latex-listings 'minted)
(setq org-latex-pdf-process '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

;; add dot to the language recognized
(org-babel-do-load-languages
 'org-babel-load-languages
 '((dot . t)))

(defun my-org-confirmation-babel-eval (lang body)
  "Auto confirm if the `LANG' is `dot' (I'm adding `BODY' here for flycheck pleasure)."
  (not (string= lang "dot")))
(setq org-confirm-babel-evaluate 'my-org-confirmation-babel-eval)

;; c
;; (c-set-offset 'case-label '+)

;; doc view
(setq doc-view-resolution 300)

;; timeout for nrepl
(setq nrepl-sync-request-timeout 50)

;; update the path
;; (let ((p "/usr/local/jdk-1.8.0/bin"))
;;   (setenv "PATH" (concat (getenv "PATH") ":" p))
;;   (setq exec-path (append exec-path (list p))))

;; c style
(setq-default c-basic-offset 8
	      tab-width 8
	      indent-tabs-mode t)

;; (define-key c-mode-map (kbd "TAB") 'self-insert-command)

;; custom mode line
;; (set-face-attribute 'mode-line nil :background "#f0544c" :foreground "#000")
(setq-default mode-line-format
      (list

       ;; line and columns
       ;; '%02' to set to 2 char at least
       " "
       (propertize "%02l" 'face '((:foreground "#093d26"))) ";"
       (propertize "%02c" 'face '((:foreground "#093d26")))
       " ::"

       evil-mode-line-tag

       ;; the buffer name
       '(:eval (propertize "%b " 'face '((:foreground "#3a2b85" :slant 'italic))
			   'help-echo (buffer-file-name)))

       ;; relative position, size of the file
       "["
       (propertize "%p" 'face '((:foreground "#def")))
       "/"
       (propertize "%I" 'face '((:foreground "#def")))
       "] "

       ;; The current major mode for the buffer
       "<"
       '(:eval (propertize "%m" 'face '((:slant 'italic :weight 'normal :foreground "#77076e"))
			   'help-echo buffer-file-coding-system))
       "> "

       "["
       '(:eval (propertize (if overwrite-mode "Ovr" "Ins")
			   'face '((:foreground "#fed"))
			   'help-echo (concat "Buffer is in"
					      (if overwrite-mode "overwrite" "insert") " mode")))

       ;; was thi buffer modified since last save
       '(:eval (when (buffer-modified-p)
		 (concat "," (propertize "Mod"
					 'face '((:foreground "#fed"))
					 'help-echo "Buffer has been modified"))))

       ;; is it read only?
       '(:eval (when buffer-read-only
		 (concat "," (propertize "RO"
					 'face '((:foreground "#fed"))
					 'help-echo "Buffer is read-only"))))
       "] "

       " --"
       minor-mode-alist
       ))

;; open file with the current major mode
(add-to-list 'auto-mode-alist
	     '("muttrc\\'" . conf-mode))

;;; custom org mode block


;; custom
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(cider-boot-parameters "repl -s -H localhost wait")
 '(custom-safe-themes
   (quote
    ("cd736a63aa586be066d5a1f0e51179239fe70e16a9f18991f6f5d99732cabb32" "151bde695af0b0e69c3846500f58d9a0ca8cb2d447da68d7fbf4154dcf818ebc" "d1b4990bd599f5e2186c3f75769a2c5334063e9e541e37514942c27975700370" "9d9fda57c476672acd8c6efeb9dc801abea906634575ad2c7688d055878e69d6" "ecba61c2239fbef776a72b65295b88e5534e458dfe3e6d7d9f9cb353448a569e" "a3fa4abaf08cc169b61dea8f6df1bbe4123ec1d2afeb01c17e11fdc31fc66379" "a566448baba25f48e1833d86807b77876a899fc0c3d33394094cf267c970749f" "93a0885d5f46d2aeac12bf6be1754faa7d5e28b27926b8aa812840fe7d0b7983" "fe666e5ac37c2dfcf80074e88b9252c71a22b6f5d2f566df9a7aa4f9bea55ef8" "b01b91ba9276deb39aa892c105a8644ef281b4d1804ab7c48de96e9c5d2aaa48" default)))
 '(electric-pair-mode t)
 '(org-agenda-files (quote ("~/agenda.org")))
 '(package-selected-packages
   (quote
    (racket-mode company-irony irony irony-mode rotate vi-tilde-fringe vi-tilde-fringe-mode go-mode haskell-mode writegood-mode editorconfig magit restclient yasnippet counsel-projectile flycheck rainbow-delimiters company js2-mode dumb-jump flx projectile counsel ivy cider clojure-mode markdown-mode web-mode evil-surround evil-cleverparens evil-commentary telephone-line doom-themes evil use-package)))
 '(safe-local-variable-values
   (quote
    ((eval setq flycheck-clang-include-path
	   (list
	    (expand-file-name "/usr/X11R6/include")
	    (expand-file-name "/usr/X11R6/include/freetype2")))
     (eval setq flycheck-clang-include-path
	   (list
	    (expand-file-name "/usr/X11R6/include")
	    (expand-file-name "/usr/include/freetype2")))
     (eval setq flycheck-clang-include-path
	   (list
	    (expand-file-name "/usr/include")
	    (expand-file-name "/usr/include/freetype2")))
     (eval setq flycheck-clang-include-path
	   (list
	    (expand-file-name "/usr/local/include/")
	    (expand-file-name "/usr/X11R6/include/")
	    (expand-file-name "/usr/X11R6/include/libdrm")))
     (eval setq flycheck-clang-include-path
	   (list
	    (expand-file-name "/usr/local/include/")))
     (eval setq flycheck-clang-include-path
	   (list
	    (expand-file-name "/usr/X11R6/include/")
	    (expand-file-name "/usr/X11R6/include/freetype2")
	    (expand-file-name "/usr/local/include/libpng16")))
     (eval setq flycheck-clang-include-path
	   (list
	    (expand-file-name "/usr/X11R6/include/")))
     (eval setq flycheck-clang-include-path
	   (list
	    (expand-file-name "/usr/X11R6/include/")
	    (expand-file-name "/usr/X11R6/include/freetype2")))))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;; init.el ends here
